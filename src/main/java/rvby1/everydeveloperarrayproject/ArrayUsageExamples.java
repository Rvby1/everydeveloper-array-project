package rvby1.everydeveloperarrayproject;

import java.util.Arrays;
import java.util.Collections;

/**
 *
 * @author Daniel
 */
public class ArrayUsageExamples {
    public static void main(String[] args) {
        //Array Definition and Element Assignment: 
        String[] beatlesNames;		//1
        beatlesNames = new String[4];	//2
        beatlesNames[0] = "John";	//3
        beatlesNames[1] = "Paul";	//4
        beatlesNames[2] = "George";	//5
        beatlesNames[3] = "Ringo";	//6
        
        //One Line New Array Definition: 
        String[] beatlesNamesOneLine = new String[4];
        
        //Literal Array Definition: 
        String[] beatlesNamesLiteral = {"John", "Paul", "George", "Ringo"};
        
        //Print Array Data: 
        System.out.println("Unsorted array:");
        printArrayData(beatlesNames);
        /*
        Output: 
            Unsorted array:
            John
            Paul
            George
            Ringo
        */
        
        //Print Ascending Order: 
        System.out.println("Array sorted in ascending order:");
        Arrays.sort(beatlesNames);
        printArrayData(beatlesNames);
        /*
        Output: 
            Array sorted in ascending order:
            George
            John
            Paul
            Ringo
        */
        
        //Print Descending Order: 
        System.out.println("Array sorted in descending order:");
        Arrays.sort(beatlesNames, Collections.reverseOrder());
        printArrayData(beatlesNames);
        /*
        Output: 
            Array sorted in descending order:
            Ringo
            Paul
            John
            George
        */
    }
    
    //Print Array Data:
    /**
     * Prints each element in arrayToPrint on a separate line to standard-out
     * @param arrayToPrint the array that should be printed to standard-out
     */
    public static void printArrayData(String[] arrayToPrint) { 
	for(int i = 0; i < arrayToPrint.length; i++) {  //1
		System.out.println(arrayToPrint[i]);	//2
	}
    }
}
